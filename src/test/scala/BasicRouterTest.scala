import routers.BasicRouter
import spray.testkit.ScalatestRouteTest
import org.scalatest._

class BasicRouterTest extends FunSuite with ScalatestRouteTest{

  val basicRouterInstance = new BasicRouter

  test("should never fail")
  {
    assert(true)
  }

  test("should handle GET request in /va path")
  {
    Get("/va")~>basicRouterInstance.route ~>check{
      responseAs[String] === "BasicRouter received GET request"
    }
  }

  test("should leave unhandled POST requests in /va path")
  {
    Post("/va")~>basicRouterInstance.route ~>check{
      assert(!handled)
    }
  }

}
