import akka.actor.Actor.Receive
import routers.BasicRouter
import spray.routing.HttpServiceActor

class BasicActor extends HttpServiceActor {

  val basicRouter = new BasicRouter

  val route = {basicRouter.route}

  override def receive: Receive = runRoute(route)
}
