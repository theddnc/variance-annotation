import akka.actor.{Props, ActorSystem}
import akka.io.IO
import akka.util.Timeout
import spray.can.Http
import scala.concurrent.duration._
import akka.pattern.ask

object Main extends App{

  implicit val actorSystem = ActorSystem("spray-can")

  implicit val timeout = Timeout(5.seconds)

  implicit val service = actorSystem.actorOf(Props[BasicActor], "demo-service")

  IO(Http) ? Http.Bind(service, interface = "localhost", port = 8080)

}
